


def arithmetic(a, difference, n):
    '''Calculates the sum of a arithmetic serie of n elements.
       An arithmetic sequence is of the form: a, a+d, a+2d, a+3d,...
       n is the number of elements in the sequence.'''
    #Get the arithmetic sequence
    sequence = [a+difference*x for x in range(n)]
    #Calculates its sum
    return sum(sequence)

def fib(n):
    ''' Calculates the n value of the fibonacci sequence'''
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-1)+fib(n-2)

